'use strict'

const express = require('express');
const app = express();
const fs = require('fs');
const Request = require('request');
const cheerio = require('cheerio');
const bodyParser = require('body-parser');

let searchParams = {
  uri: 'http://moviestape.net',
  method: 'GET',
  encoding: 'utf-8',
  json: true
};

function downloadMovie(url, callback = () => {}) {
  /*Request.head(url)
    .on('error', err => {
      console.error(err);
    })
    .on('response', res => {
      console.log(res.statusCode);
      console.log(res.headers['content-type']);
    })
    .pipe(fs.createWriteStream(url.split('/')[url.split('/').length - 1]))
    .on('finish', () => {
      console.log('Download process...!');
    })
    .on('close', () => {
      console.log('Download success!');
      callback();
    });*/
    Request.get(url)
      .on('error', function(err) {
        console.error(err);
      })
      .on('response', function(res) {
        console.log(res.statusCode);
        console.log(res.headers['content-type']);
      })
      .pipe(fs.createWriteStream(url.split('/')[url.split('/').length - 1]))
      .on('finish', function() {
        console.log('OK');
        callback();
      });
}

function getPlaylistLink(iframeLink) {
  const $ = cheerio.load(iframeLink.page);
  let filePath = $.html();
  let playlistLink = Cut(filePath, { fragmentStart: 'var flashvars = ', fragmentEnd: '\'};' });
  // delete spaces
  playlistLink = playlistLink.replace(/\s+/g, ' ');

  let type;

  if (playlistLink.indexOf(', pl : ') !== -1) {
    // serial
    playlistLink = Cut(playlistLink, { fragmentStart: ', pl : \'' });
    type = 'serial';
  } else if (playlistLink.indexOf(', file : ') !== -1) {
    // movie
    playlistLink = Cut(playlistLink, { fragmentStart: ', file : \'' });
    type = 'movie';
  } else {
    console.log('Can`t find movie link');
    playlistLink = '';
  }

  return {link: playlistLink, type};
}

const fetchGet = (url) => (
  new Promise((resolve, reject) => {
    Request.get(url, (error, response, page) => {
      if (error) {
        return reject(error);
      }

      resolve({ response, page });
    });
  })
);

const getMoviePage = properties => fetchGet(properties)
.then(({ page }) => {
  const $ = cheerio.load(page);

  return $('iframe').attr('src');
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/', (req, res) => {
  res.sendFile(`${__dirname}/index.html`);
});

app.post('/', (req, res) => {
  console.log(req.body);

  searchParams.uri = req.body.uri;

  Promise.resolve(getMoviePage(searchParams))
    .then(fetchGet)
    .then(iframe => {
      let playlistlink = getPlaylistLink(iframe);
      console.log(`playlistlink: ${playlistlink.link}; type: ${playlistlink.type}`);

      if (playlistlink.type == 'serial') {
        let playlist = fetchGet(playlistlink.link).then(playlist => {
          console.log(playlist.page);
          return {playlist: playlist.page, type: playlistlink.type};
        });

        return playlist;
      } else {
        let movie = {file: playlistlink.link, type: playlistlink.type};
        return movie;
      }
    })
    .then(data => {
      res.send({status: 'Done!', data});
    })
    .catch(console.error);
});

app.post('/download', (req, res) => {
  console.log(req.body);

  downloadMovie(req.body.url, () => {
    res.send({status: 'Done!'});
  });
});

app.listen(3000, () => {
  console.log('Start server!');
});

function Cut(str, params = { fragmentStart: '', fragmentEnd: '' }) {
  let text = str;
  let flen = params.fragmentStart.length;

  if (params.fragmentEnd !== '') {
    text = text.substring(text.indexOf(params.fragmentStart) + flen, text.length);
  }

  if (params.fragmentEnd !== '') {
    text = text.split(params.fragmentEnd)[0];
  }

  return text;
}
